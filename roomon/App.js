import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import MainNavigation from './src/pages/Navigation/MainNavigation'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import store from "./src/store";

function App() {
  // // Set an initializing state whilst Firebase connects
  // const [initializing, setInitializing] = useState(true);
  // const [user, setUser] = useState();

  // // Handle user state changes
  // function onAuthStateChanged(user) {
  //   setUser(user);
  //   if (initializing) setInitializing(false);
  // }

  // useEffect(() => {
  //   const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
  //   return subscriber; // unsubscribe on unmount
  // }, []);

  // if (initializing) return null;

  // if (!user) {
    return (
      <Provider store={store}>
        <MainNavigation />
      </Provider>
    )
  // }
}

export default App;