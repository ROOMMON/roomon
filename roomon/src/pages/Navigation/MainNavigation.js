import React, { Component, useState, useEffect } from 'react';
import Image from 'react-native';

// import { StackNa} from 'react-native';
import {useSelector, useDispatch} from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth';

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet } from 'react-native';

import SignUp from '../SignInUp/SignUp';
import LogIn from '../SignInUp/LogIn';
import Congratulations from '../SignInUp/Congratulation';
import Tutorials from '../SignInUp/Tutorials';
import AfterSignUp from '../SignInUp/AfterSignUp';
import Agreement from '../SignInUp/Agreement';
import Home from '../SignInUp/Home';
import Search from '../SignInUp/Search';
import EnterNumber from '../SignInUp/EnterNumber';
import appLoadingPage from '../Loading/appLoadingPage';
import MainPage from '../MainPage/MainPage';
import MyListing from '../SignInUp/MyListing';

import Settings from '../SignInUp/Settings';
import Profile from '../SignInUp/Profile';

const Stack = createStackNavigator();
const FINDER = {
    type: "finder",
    first_name:"Alice",
    last_name:"Lee",
    age:"23",
}

const REALTOR = {
    type: "realtor",
    first_name:"Brain",
    last_name:"Oh",
    age:"42",
}

function MainNavigation() {
    const dispatch= useDispatch()
    const [hasAgreed, setHasAgreed] = useState(false)
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [isUserLoggedIn, setIsUserLoggedIn] = useState(false)
    
    useEffect(() => {
        auth().onAuthStateChanged((user) => {
          if (user) {
            const uid = user.uid
            const email = user.email
            const name = "minjae"
            const isLoggedIn = true
            dispatch.user._setUser({uid, email, name, isLoggedIn})
            // console.log("========================")
            // console.log(user)
            // console.log("set isLoggedIn = true")
            setIsUserLoggedIn(true)
            // console.log("========================")
            _saveUserTokenInDevice()
          } else {
            setIsUserLoggedIn(false)
            _resetUserTokenInDevice()
            dispatch.user._setUser({"isLoggedIn": false})
            console.info("LogIn.onAuthStateChanged: No user has found")
          }
          _getDeviceStorage()
        })
      })
      const _saveUserTokenInDevice = async () => {
        try {
          await AsyncStorage.setItem(
            'isLoggedIn',
            JSON.stringify({isLoggedIn: true}),
          )
          
        } catch (e) {
          console.error("ERROR: Agreement._saveUserTokenInDevice() message = ", e)
        }
      }
      const _resetUserTokenInDevice = async () => {
        try {
          await AsyncStorage.setItem(
            'isLoggedIn',
            JSON.stringify({isLoggedIn: false}),
          )
          
        } catch (e) {
          console.error("ERROR: Agreement._resetUserTokenInDevice() message = ", e)
        }
      }

    const _getDeviceStorage = async () => {
      
        // await AsyncStorage.setItem(
        //   'agreement',
        //   JSON.stringify({ agreement: false}),
        // );
        try {
          await AsyncStorage.multiGet(['agreement', 'isLoggedIn'], (err, result) => {
            if(result){
              result.map((result, i, store) => {
                // get at each store's key/value so you can work with it
                let key = store[i][0];
                let value = store[i][1];
                if(value && key) {
                  if(key === 'agreement'){
                    const parse = JSON.parse(value)
                    if(parse) {
                      // console.info("agreement status: ",parse.agreement)
                      setHasAgreed(parse.agreement)
                    }else {
                      console.error("ERROR: Agreement._getDeviceStorage parse.agreement should not be nul")
                    }
                  } else if(key === 'isLoggedIn'){
                    const parse=JSON.parse(value)
                    if(parse) {
                      setIsLoggedIn(parse.isLoggedIn)
                    }else {
                      console.error("ERROR: Agreement._getDeviceStorage parse.isLoggedIn should not be nul")
                    }
                  }
                }
              });
            }
          })
        } catch (e) {
          console.log("ERROR: Agreement._getDeviceStorage: ", e)
        }
          
      }

  return (
    <NavigationContainer>
        <Stack.Navigator >
            {isUserLoggedIn ? (
                <>
                    <Stack.Screen name="MyListing" component={MyListing} options={{headerShown: false}}/>
                <Stack.Screen name="Profile" component={Profile} options={{headerShown: false}}/>
                <Stack.Screen name="Settings" component={Settings} options={{headerShown: false}}/>
                    <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/>
                    {/* <Stack.Screen name="Main Navigator" component={MainNavigation} options={{headerShown: false}}/> */}
                    <Stack.Screen name="LogIn" component={ LogIn }  options={{headerShown: false}} />
                    <Stack.Screen name="SignUp" component={ SignUp }  options={{headerShown: false}} />
                    <Stack.Screen name="EnterNumber" component={EnterNumber} options={{headerShown: false}}/>
                            <Stack.Screen name="AfterSignUp" component={AfterSignUp} options={{headerShown: false}}/>
                            <Stack.Screen name="Congratulations" component={Congratulations} options={{headerShown: false}} />
                </>
            ) : (
                <>
                    {/* {hasAgreed ? (
                        <> */}
                            <Stack.Screen name="MyListing" component={MyListing} options={{headerShown: false}}/>
                        {/* <Stack.Screen name="Profile" component={Profile} options={{headerShown: false}}/> */}
                        <Stack.Screen name="Settings" component={Settings} options={{headerShown: false}}/>
                            <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/>
                            {/* <Stack.Screen name="Agreement" component={ Agreement } options={{headerShown: false}}/>
                            <Stack.Screen name="LogIn" component={ LogIn }  options={{headerShown: false}} />
                            <Stack.Screen name="SignUp" component={ SignUp }  options={{headerShown: false}} />
                            <Stack.Screen name="EnterNumber" component={EnterNumber} options={{headerShown: false}}/>
                            <Stack.Screen name="AfterSignUp" component={AfterSignUp} options={{headerShown: false}}/>
                            <Stack.Screen name="Congratulations" component={Congratulations} options={{headerShown: false}} />
                            <Stack.Screen name="City" options={{headerShown: false}}>
                                {props => <Search {...props} pageType={"City"} />}
                            </Stack.Screen>

                            <Stack.Screen name="Skytrain" options={{headerShown: false}}>
                                {props => <Search {...props} pageType={"Skytrain"} />}
                            </Stack.Screen>

                            <Stack.Screen name="Add School" options={{headerShown: false}}>
                                {props => <Search {...props} pageType={"Add School"} />}
                            </Stack.Screen>

                            <Stack.Screen name="Add Work" options={{headerShown: false}}>
                                {props => <Search {...props} pageType={"Add Work"} />}
                            </Stack.Screen>
                            <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/> */}
                </>
            )}
            
            {/* <Stack.Screen name="appLoadingPage" component={ appLoadingPage } options={{headerShown: false}} /> */}
            
            {/* <Stack.Screen name="Agreement" component={ Agreement } options={{headerShown: false}}/> */}
            {/* <Stack.Screen name="MainPage" component={ MainPage } options={{headerShown: false}} /> */}
            {/* <Stack.Screen name="LogIn" component={ LogIn }  options={{headerShown: false}} />
            <Stack.Screen name="SignUp" component={ SignUp }  options={{headerShown: false}} />
            <Stack.Screen name="EnterNumber" component={EnterNumber} options={{headerShown: false}}/>
            <Stack.Screen name="AfterSignUp" component={AfterSignUp} options={{headerShown: false}}/>
            <Stack.Screen name="Congratulations" component={Congratulations} options={{headerShown: false}} /> */}
            

            {/* <Stack.Screen name="City" options={{headerShown: false}}>
                {props => <Search {...props} pageType={"City"} />}
            </Stack.Screen>

            <Stack.Screen name="Skytrain" options={{headerShown: false}}>
                {props => <Search {...props} pageType={"Skytrain"} />}
            </Stack.Screen>

            <Stack.Screen name="Add School" options={{headerShown: false}}>
                {props => <Search {...props} pageType={"Add School"} />}
            </Stack.Screen>

            <Stack.Screen name="Add Work" options={{headerShown: false}}>
                {props => <Search {...props} pageType={"Add Work"} />}
            </Stack.Screen> */}
        </Stack.Navigator>
    </NavigationContainer>
  );
}

export default MainNavigation;