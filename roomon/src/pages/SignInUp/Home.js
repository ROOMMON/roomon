import React, { useState, useEffect } from 'react';
import {Animated, Text, View, StyleSheet, TextInput, Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback, Dimensions, ImageBackground} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types'
import {useSelector, useDispatch} from 'react-redux'
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/native';



import LinearGradient from 'react-native-linear-gradient';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;



//Reusable components
import DefaultContainer from '../../components/DefaultContainer';
import Card from '../../components/Card';

const assestsPath = "../../../assets/Home/";



const MenuView = React.memo(function MenuView(props){
    const [slideAnim] = useState(new Animated.Value(-1000));  // Initial value for position
    
      useEffect( () => {
        if(props.openMenu)
        {
            slideAnim.setValue(-1000)
            console.log('props.openMenu updated', props.openMenu, slideAnim);
            Animated.timing(
                slideAnim,
                {
                  toValue: 0,
                  duration: 250,
                  useNativeDriver: false
                }
              ).start();
        }
        else
        {
            slideAnim.setValue(0)
            console.log('props.openMenu updated', props.openMenu, slideAnim);
            Animated.timing(
                slideAnim,
                {
                  toValue: -1000,
                  duration: 250,
                  useNativeDriver: false
                }
              ).start();
        }
    }, [props.openMenu])


      return (
        <Animated.View
          style={{
            ...props.style,
            right: slideAnim
          }}
        >
          {props.children}
        </Animated.View>
      );
    }
);

export default Home = React.memo(function Home(props){

    const dispatch = useDispatch()
    const [openMenu, setOpenMenu] = useState(false);

    const userUid = useSelector((state) => state.user.uid)
    const useEmail = useSelector((state) => state.user.email)
    const username = useSelector((state) => state.user.name)
    const [isLoggedIn, setIsLoggedIn] = useState(useSelector((state) => state.user.isLoggedIn))
    
    useEffect(()=>{
        console.log("========123================")
        
        console.log("useSelector = ", userUid,"email: ", useEmail,"name: ", username,"isloggedIn: ", isLoggedIn)
        console.log("=========123===============")
    },[])

    const onLogOut = async () =>{
        // await AsyncStorage.clear()
        try {
            await AsyncStorage.removeItem('isLoggedIn')
            setIsLoggedIn(false)
            dispatch.user._setUser({'isLoggedIn': false})
          } catch(e) {
            console.error("ERROR: Home.onLogOut, message= ", e)
        }
        auth()
        .signOut()
        .then(
            props.navigation.dispatch(
                CommonActions.reset({
                  index: 0,
                  routes: [
                    {
                      name: 'LogIn',
                    }
                  ],
                })
              )
        )
        
    }

    const renderHamburgerMenu = () =>{
        return(
            [
                <TouchableWithoutFeedback onPress={()=>setOpenMenu(false)}>
                    <View style={{
                    zIndex:openMenu?2:0,
                    position:"absolute", 
                    opacity:openMenu?0.5:0, 
                    backgroundColor:"black", 
                    top:0, 
                    left:0, 
                    height:windowHeight, 
                    width:windowWidth, 
                    display:openMenu?"flex":"none"
                    }}/>
                </TouchableWithoutFeedback>,

                <MenuView openMenu={openMenu} style={{backgroundColor:"#fff", position:"absolute", width:windowWidth*0.80, height:windowHeight-(40), top:40, zIndex:10}}>
                        {/* <ImageBackground source={require(assestsPath+"bgWithShade.png")} style={{height:140, width:"100%", bottom:1}}/> */}
                        <View 
                            style={{height:140, width:"100%", backgroundColor:"#5c42dd", padding:15, flexDirection:"row",         justifyContent:"space-between"}}>
                            <View style={{flexDirection:"column", justifyContent:"space-between"}}>
                                <Image style={{marginBottom:6}} source={require(assestsPath+"profile.png")}/>
                                <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:16, color:"#fff"}}>{`Alice Lee`}</Text>
                                <Text style={{fontFamily:"Roboto-Regular", fontSize:12, color:"#fff"}}>{`aliceLee@gmail.com`}</Text>
                            </View>
                            <View style={{flexDirection:"row",  alignSelf:"flex-start", alignContent:"center", alignItems:"center", }}>
                                <TouchableOpacity>
                                    <Image source={require(assestsPath+"messengerBox.png")}/>
                                </TouchableOpacity>
                                <View style={{height:"100%", width:15}}/>
                                <TouchableOpacity>
                                    <Image source={require(assestsPath+"setting.png")}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View 
                            style={{height:windowHeight-140-40, width:"100%", flexDirection:"column", paddingLeft:31, paddingRight:19, paddingBottom:windowHeight*0.14, paddingTop:windowHeight*0.045}}>
                            
                            <View style={{height:windowHeight*0.57, width:"100%", alignSelf:"center", flexDirection:"column"}}>

                                <TouchableOpacity style={{flexDirection:"row", marginBottom:30, alignContent:"center", alignItems:"center", flexDirection:"row"}}>
                                    <View style={{width:31}}>
                                        <Image source={require(assestsPath+"home.png")}/>
                                    </View>
                                    <View style={{height:1, width:15}}/>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:16, color:"#5c42dd"}}>{`Home`}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{flexDirection:"row", marginBottom:30, alignContent:"center", alignItems:"center", flexDirection:"row"}}>
                                    <View style={{width:31}}>
                                        <Image source={require(assestsPath+"favorite.png")}/>
                                    </View>
                                    <View style={{height:1, width:15}}/>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:16, color:"#252533"}}>{`Favorite`}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{flexDirection:"row", marginBottom:30, alignContent:"center", alignItems:"center", flexDirection:"row"}}>
                                    <View style={{width:31}}>
                                        <Image source={require(assestsPath+"myRentals.png")}/>
                                    </View>
                                    <View style={{height:1, width:15}}/>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:16, color:"#252533"}}>{`My Rentals`}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{flexDirection:"row", marginBottom:17, alignContent:"center", alignItems:"center", flexDirection:"row"}}>
                                    <View style={{width:31}}>
                                        <Image source={require(assestsPath+"postListing.png")}/>
                                    </View>
                                    <View style={{height:1, width:15}}/>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:16, color:"#252533"}}>{`Post Listing`}</Text>
                                </TouchableOpacity>

                                <View style={{borderWidth:0.5, width:"100%", borderColor:"#d4d4d4"}}/>

                                <TouchableOpacity style={{flexDirection:"row", height:26, marginBottom:30, marginTop:19, alignContent:"center", alignItems:"center", flexDirection:"row"}}>
                                    <View style={{width:31}}>
                                        <Image source={require(assestsPath+"report.png")}/>
                                    </View>
                                    <View style={{height:1, width:15}}/>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:16, color:"#252533"}}>{`Report`}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{flexDirection:"row", height:26, marginBottom:17, alignContent:"center", alignItems:"center"}}>
                                    <View style={{width:31, left:3}}>
                                        <Image source={require(assestsPath+"help.png")}/>
                                    </View>
                                    <View style={{height:1, width:15}}/>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:16, color:"#252533"}}>{`Help & FAQ`}</Text>
                                </TouchableOpacity>

                                <View style={{borderWidth:0.5, width:"100%", borderColor:"#d4d4d4"}}/>

                                <TouchableOpacity style={{marginLeft:46, flexDirection:"row", height:14, marginTop:15}}>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:14, color:"#898989"}}>{`About Us`}</Text>
                               </TouchableOpacity>

                               <TouchableOpacity style={{marginLeft:46, flexDirection:"row", height:16, marginTop:15}}>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:14, color:"#898989"}}>{`Terms of Service`}</Text>
                               </TouchableOpacity>

                               <TouchableOpacity 
                                onPress={()=>onLogOut()}
                                style={{marginLeft:46, flexDirection:"row", height:16, marginTop:15}}>
                                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:14, color:"#898989"}}>{`Log out`}</Text>
                               </TouchableOpacity>
                                
                            </View>
                        </View>
                </MenuView>
                , 
            ]
        );
    }

    const destinationButton = (label, userChoice) =>{
        let imgURL;
        if(label === "School")
            imgURL = require(assestsPath+`${"school"}.png`);
        else if(label === "Work")
            imgURL = require(assestsPath+`${"work"}.png`);
        else
            imgURL = require(assestsPath+`${"skytrain"}.png`);

        return(
            <TouchableOpacity style={{paddingLeft:15, paddingRight:14, marginBottom:16, marginTop:20, flexDirection:"row", justifyContent:"space-between", alignContent:"center", alignItems:"center"}}>
                <View style={{flexDirection:"row",}}>
                    <Image style={{}} source={imgURL}/>
                        <View style={{flexDirection:"column", justifyContent:"center", marginLeft:8}}>
                            <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:18, color:"#252533"}}>{label}</Text>
                            <Text style={{fontFamily:"Roboto-Regular", fontSize:16, color:"#252533"}}>{userChoice}</Text>
                        </View>
                </View>
                <Image source={require(assestsPath+"arrowRight.png")}/>
            </TouchableOpacity>
        );
    };

    const renderRecommendedProperties = () =>{
        return(
            <TouchableOpacity style={{flexDirection:"column", marginRight:20}}>

                <View style={{height:175, width:240, backgroundColor:"#e8e8e8"}}/>

                <View style={{width:240, height:18, flexDirection:"row", alignContent:"center", alignItems:"center", justifyContent:"space-between", marginTop:7}}>
                    <Text style={{color:"#fff"}}>{`2 Bed`}</Text>
                    <View style={{borderRightWidth:0.5, borderColor:"#dddddd", height:"100%"}}/>

                    <Text style={{color:"#fff"}}>{`1 Bath`}</Text>
                    <View style={{borderRightWidth:0.5, borderColor:"#dddddd", height:"100%"}}/>

                    <Text style={{color:"#fff"}}>{`Feb 1`}</Text>
                    <View style={{borderRightWidth:0.5, borderColor:"#dddddd", height:"100%"}}/>

                    <Text style={{color:"#fff"}}>{`2100 sq`}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    const renderNearbyProperties = () =>{
        return(
            <TouchableOpacity style={{flexDirection:"column", marginRight:20}}>

                <View style={{height:175, width:240, backgroundColor:"#e8e8e8"}}/>

                <View style={{width:240, height:18, flexDirection:"row", alignContent:"center", alignItems:"center", justifyContent:"space-between", marginTop:7}}>
                    <Text style={{color:"#252533"}}>{`2 Bed`}</Text>
                    <View style={{borderRightWidth:0.5, borderColor:"#dddddd", height:"100%"}}/>

                    <Text style={{color:"#252533"}}>{`1 Bath`}</Text>
                    <View style={{borderRightWidth:0.5, borderColor:"#dddddd", height:"100%"}}/>

                    <Text style={{color:"#252533"}}>{`Feb 1`}</Text>
                    <View style={{borderRightWidth:0.5, borderColor:"#dddddd", height:"100%"}}/>

                    <Text style={{color:"#252533"}}>{`2100 sq`}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    const renderCityPanels = () =>{
        const cities = ["Burnaby", "Coquitlam", "Vancouver", "City", "City", "City", "City"];
        let cityPanels = cities.map((city)=>{
            return(
                <TouchableOpacity style={{flexDirection:"column", marginRight:20}}>
    
                    <View style={{height:175, width:240, backgroundColor:"#e8e8e8", justifyContent:"center", alignContent:"center", alignItems:"center"}}>
                        <Text style={{fontFamily:"Archivo-Bold", fontSize:24, color:"#252533"}}>
                            {city}
                        </Text>
                    </View>

                </TouchableOpacity>
            );
        });
        return cityPanels;
    }

    const renderHomePage = () =>{
        const hamburger = renderHamburgerMenu();
            return(
                [                   
                    ...hamburger,
                <KeyboardAwareScrollView 
                showsVerticalScrollIndicator={false}
                style={{flex:1, height: '100%'}} 
                extraScrollHeight={30} 
                enableOnAndroid>
                    <View style={{...styles.container}}>

                        <View style={{...styles.topBar}}>
                            <Image source={require(assestsPath+"logo.png")}/>

                            <TouchableOpacity  onPress={()=>setOpenMenu(true)}>
                                <Image source={require(assestsPath+"hamburger.png")}/>
                            </TouchableOpacity>
                        </View>

                        <Text style={{fontSize:22, fontFamily:"ArchivoBlack-Regular", color:"#5c42dd", marginBottom:15}}>
                            {`Let’s search for your home!`}
                        </Text>

                        <Text style={{fontSize:16, fontFamily:"Roboto-Light", color:"#252533", marginBottom:30,}}>
                            {`Enter your phone number below and we’ll send you a link to reset your password`}
                        </Text>

                        <View>
                            <Image style={{position:"absolute", top:8, left:12}} source={require(assestsPath+"searchbar.png")}/>
                            <TextInput style={{...styles.searchBar, fontSize:16}} placeholder={`Search location, or address`}/>
                        </View>

                        <View style={{flexDirection:"column"}}>

                            <View style={{paddingBottom:11, borderBottomWidth:1, borderBottomColor:"#d1d1d1", ...styles.flexRowAlignCenter, justifyContent:"space-between"}}>
                                <Text style={{fontSize:22, fontFamily:"Archivo-Bold", color:"#252533"}}>
                                    {`My Destinations`}
                                </Text>
                                <TouchableOpacity>
                                    <Text style={{fontSize:14, fontFamily:"Roboto-Black", color:"#b8b8cc", alignSelf:"center", top:5}}>
                                        {`Edit`}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                            {destinationButton("School", "Simon Fraser University")}
                            <View style={{borderWidth:0.5, borderColor:"#d8d8d8"}}/>
                            {destinationButton("Work", "Sushi California")}
                            <View style={{borderWidth:0.5, borderColor:"#d8d8d8"}}/>
                            {destinationButton("Skytrain", "Lougheed Station")}

                            <View style={{width:windowWidth, height:15}}/>


                            <View 
                            style={{height:350, backgroundColor:"#5c42dd", width:windowWidth, alignSelf:"center", right:0.3, flexDirection:"column", paddingTop:52, paddingBottom:40, paddingLeft:16}}>
                            
                                <View style={{borderBottomWidth:1, borderBottomColor:"#fff", paddingBottom:3, marginBottom:20, marginRight:15}}>
                                    <Text style={{fontFamily:"Archivo-Bold", fontSize:24, color:"#fff"}}>{"Recommended Properties"}
                                    </Text>
                                </View>

                                <ScrollView 
                                horizontal 
                                showsHorizontalScrollIndicator={false}
                                style={{width:"100%",  alignSelf:"center",}}>
                                    {renderRecommendedProperties()}
                                    {renderRecommendedProperties()}
                                    {renderRecommendedProperties()}
                                    {renderRecommendedProperties()}
                                </ScrollView>
                            </View>


                            <View 
                            style={{height:320, width:windowWidth, alignSelf:"center", right:0.3, flexDirection:"column", paddingTop:30, paddingBottom:32, paddingLeft:16,}}>
                            
                                <View style={{borderBottomWidth:1, borderBottomColor:"#252533", paddingBottom:3, marginBottom:20, marginRight:15}}>
                                    <Text style={{fontFamily:"Archivo-Bold", fontSize:24, color:"#252533"}}>
                                        {"Popular Properties"}
                                    </Text>
                                </View>

                                <ScrollView 
                                horizontal 
                                showsHorizontalScrollIndicator={false}
                                style={{width:"100%",  alignSelf:"center"}}>
                                    {renderNearbyProperties()}
                                    {renderNearbyProperties()}
                                    {renderNearbyProperties()}
                                    {renderNearbyProperties()}
                                </ScrollView>
                            </View>

                            <View 
                            style={{height:320, width:windowWidth, alignSelf:"center", right:0.3, flexDirection:"column", paddingBottom:32, paddingLeft:16,}}>
                            
                                <View style={{borderBottomWidth:1, borderBottomColor:"#252533", paddingBottom:3, marginBottom:20, marginRight:15}}>
                                    <Text style={{fontFamily:"Archivo-Bold", fontSize:24, color:"#252533"}}>
                                        {"Nearby Properties"}
                                    </Text>
                                </View>

                                <ScrollView 
                                horizontal 
                                showsHorizontalScrollIndicator={false}
                                style={{width:"100%",  alignSelf:"center",}}>
                                    {renderNearbyProperties()}
                                    {renderNearbyProperties()}
                                    {renderNearbyProperties()}
                                    {renderNearbyProperties()}
                                </ScrollView>
                            </View>


                            <View 
                            style={{height:320, width:windowWidth, alignSelf:"center", right:0.3, flexDirection:"column", paddingBottom:32, paddingLeft:16,}}>
                            
                                <View style={{borderBottomWidth:1, borderBottomColor:"#252533", paddingBottom:3, marginBottom:20, marginRight:15}}>
                                    <Text style={{fontFamily:"Archivo-Bold", fontSize:24, color:"#252533"}}>
                                        {"City"}
                                    </Text>
                                </View>

                                <ScrollView 
                                horizontal 
                                showsHorizontalScrollIndicator={false}
                                style={{width:"100%",  alignSelf:"center",}}>
                                    {renderCityPanels()}
                                </ScrollView>
                            </View>

                        </View>
                        
                    </View>
                </KeyboardAwareScrollView>]
            );
    };

    
    return renderHomePage();
});

Home.propTypes = {
    user: PropTypes.object,

  };

Home.defaultProps = {
    user: {
        type: "guest",
        first_name:"User",
        last_name:"",
        age:"",
    },
  };

const styles = StyleSheet.create({
    container:{
        display:"flex",
        paddingTop:40,
        paddingLeft:16,
        paddingRight:15,
        
    },
    topBar:{
        display:"flex",
        flexDirection:"row",
        alignContent:"center",
        alignItems:"center",
        justifyContent:"space-between",
        marginBottom:20,
    },
    searchBar:{
        width:"100%",
        paddingTop:13,
        paddingBottom:11,
        paddingRight:5,
        paddingLeft:50,
        marginBottom:22,
        borderWidth:1,
        borderColor:"#d1d1d1",
    },
    flexRowAlignCenter:{
        display:"flex",
        flexDirection:"row",
        alignContent:"center",
        alignItems:"center",
    }
});