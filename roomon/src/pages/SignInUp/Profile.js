import React, { useState, useEffect } from 'react';
import {Switch, Animated, Text, View, StyleSheet, TextInput, Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback, Dimensions, ImageBackground} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types'



const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;


const assestsPath = "../../../assets/Settings/";
const cblack = "#252533";
const cpurple = "#5c42dd";

const Settings = () => {

    const renderProfileRow = (label="", displayArrow=true, displayBorderBottom=true,) =>{
        return(
            [<TouchableOpacity style={{width:"100%", height:26, flexDirection:"row", backgroundColor:"#fff", alignContent:"center", alignItems:"center", justifyContent:"space-between", marginBottom:20}}>
                <View style={{flexDirection:"row", height:"100%", alignContent:"center", alignItems:"center"}}>
                    <Text style={{fontFamily:"Roboto-Light", fontSize:18,  color:cblack}}>{label}</Text>
                </View>
                <Image source={require(assestsPath+"arrowBlue.png")}/>
            </TouchableOpacity>,
            <View style={{width:"100%", height:1, backgroundColor:"#d8d8d8", display:displayBorderBottom?"flex":"none", marginBottom:20}}/>]
        );
    }
    return (
        [    <View style={{...styles.topBar}}>
                <TouchableOpacity>
                    <Image style={{position:"absolute", bottom:0, left:16}} source={require(assestsPath+"back.png")}/>
                </TouchableOpacity>
                <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:20, color:cblack,}}>
                    Profile
                </Text>
                <View style={{height:"100%", width:10}}/>
            </View>,
            <View style={{width:windowWidth, height:233, flexDirection:"column", alignContent:"center", alignItems:"center", justifyContent:"flex-start", backgroundColor:"#fff", paddingTop:20}}>
                <View style={{height:142, width:142, borderWidth:4, borderColor:"#fff", borderRadius:100, backgroundColor:"#8b8b8b", ...styles.genericShadow, alignContent:"center", alignItems:"center", justifyContent:"center", marginBottom:25}}>
                    <Text style={{fontFamily:"Roboto-Bold", fontSize:16, color:"#fff"}}>
                        Add Photo
                    </Text>
                    <TouchableOpacity style={{position:"absolute", bottom:-8, right:8, borderWidth:4, borderColor:"#fff", borderRadius:50}}>
                      <Image style={{ height:30, width:30}} source={require(assestsPath+"plusActive.png")}/>
                    </TouchableOpacity>
                </View>
                <Text style={{color:cblack, fontFamily:"ArchivoBlack-Regular", fontSize:22,}}>
                    {`Francesca Farago`}
                </Text>
            </View>,

        <ScrollView style={{...styles.container, backgroundColor:"#e8e8e8"}} showsVerticalScrollIndicator={false}>
            <View style={{height:56, width:"100%", color:cpurple, backgroundColor:"#e8e8e8",flexDirection:"column", alignContent:"center", alignItems:"flex-start", justifyContent:"flex-end", paddingBottom:10, paddingLeft:16}}>
                <Text style={{color:cblack, fontFamily:"Roboto-Regular", fontSize:12, letterSpacing:1.5}}>{`Account Settings`}</Text>
            </View>
            <View style={{paddingLeft:30, paddingRight:30, backgroundColor:"#fff", width:windowWidth, flexDirection:"column", alignContent:"center", paddingTop:18}}>
                {renderProfileRow("Personal Information")}
                {renderProfileRow("My Applications")}
                {renderProfileRow("Payments", true, false)}
            </View>

            <View style={{height:56, width:"100%", color:cpurple, backgroundColor:"#e8e8e8", flexDirection:"column", alignContent:"center", alignItems:"flex-start", justifyContent:"flex-end", paddingBottom:10, paddingLeft:16}}>
                <Text style={{color:cblack, fontFamily:"Roboto-Regular", fontSize:12, letterSpacing:1.5}}>{`My Activity`}</Text>
            </View>
            <View style={{paddingLeft:30, paddingRight:30, backgroundColor:"#fff", width:windowWidth, flexDirection:"column", alignContent:"center", paddingTop:18, marginBottom:10}}>
                {renderProfileRow("My Contract", true, false)}
            </View>
        
        </ScrollView>]
    )
}

export default Settings;

const styles = StyleSheet.create({
    container:{
        height:windowHeight,
        width:windowWidth,
        display:"flex",
        // justifyContent:"flex-start",
        flexDirection:"column",
    },
    topBar:{
        height:75,
        backgroundColor:"#fff",
        display:"flex",
        justifyContent:"space-between",
        flexDirection:"row",
        alignItems:"flex-end",
        alignContent:"center",
        paddingBottom:6,
    },
    genericShadow:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    }
});
