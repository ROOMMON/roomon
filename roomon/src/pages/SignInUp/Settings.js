import React, { useState, useEffect } from 'react';
import {Switch, Animated, Text, View, StyleSheet, TextInput, Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback, Dimensions, ImageBackground} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types'



const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;


const assestsPath = "../../../assets/Settings/";
const cblack = "#252533";
const cpurple = "#5c42dd"

const userInfo = {
    name:"Francesca Fargo",
    govId:"unverified",
    phone:"+1 778-888-5533",
    notifPref:true,
}

const Settings = () => {

    const [notif, setNotif] = useState(false);

    const handleNotifToggle = () => setNotif(!notif);

    const handleSliderAndArrow = (label="", displayArrow=true) =>{
        if(displayArrow)
        {
            return <Image source={require(assestsPath+"arrowBlue.png")}/>;
        }
        if(!displayArrow && label === "Notifications")
        {
            return (
                <Switch
                trackColor={{ false: "#ebebeb", true: cpurple}}
                thumbColor={notif ? "#fff" : "#ebebeb"}
                ios_backgroundColor={"#ebebeb"}
                onValueChange={handleNotifToggle}
                value={notif}
                />
            );

        }
        return null;
    }

    const handleLabels = (label="") =>{
        if(label === "id")
        {
            return(
                <View style={{flexDirection:"column", width:180, height:"100%", alignSelf:"center", bottom:5}}>
                    <Text style={{fontFamily:"Roboto-Light", fontSize:18,  color:cblack}}>
                        {`Government I.D.`}
                    </Text>
                    <Text style={{fontFamily:"Roboto-Regular", fontSize:12,  color:"#969696"}}>
                        {`Unverified`}
                    </Text>
                </View>
            );
        }
        return (
            <Text style={{fontFamily:"Roboto-Light", fontSize:18,  color:cblack}}>
                {label}
            </Text>
        );
    }

    const renderSettingsRowUserInfo = (label="", displayArrow=true, displayBorderBottom=true, displayIcon=true) =>{
        return(
            [<TouchableOpacity style={{width:"100%", height:26, flexDirection:"row", backgroundColor:"#fff", alignContent:"center", alignItems:"center", justifyContent:"space-between", marginBottom:20}}>
                <View style={{flexDirection:"row", height:"100%", alignContent:"center", alignItems:"center"}}>
                    <View style={{height:25, width:25, borderWidth:1, borderRadius:50, backgroundColor:"#ebebeb", borderColor:"#ebebeb", marginRight:13, display:displayIcon?"flex":"none",}}/>
                    {handleLabels(label)}
                </View>
                {handleSliderAndArrow(label, displayArrow)}
            </TouchableOpacity>,
            <View style={{width:"100%", height:1, backgroundColor:"#d8d8d8", marginBottom:20, display:displayBorderBottom?"flex":"none"}}/>]
        );
    }

    const renderSettingsRowAppInfo = (label="", displayArrow=true, displayBorderBottom=true) =>{
        return(
            [<TouchableOpacity style={{width:"100%", height:21, flexDirection:"row", backgroundColor:"#fff", alignContent:"center", alignItems:"center", justifyContent:"space-between", marginBottom:windowHeight*0.016, marginTop:windowHeight*0.016}}>
                <View style={{flexDirection:"row", height:"100%", alignContent:"center", alignItems:"center"}}>
                    <Text style={{fontFamily:"Roboto-Light", fontSize:18,  color:cblack}}>
                        {label}
                    </Text>
                </View>
                <Image style={{display:displayArrow?"flex":"none"}} source={require(assestsPath+"arrowBlue.png")}/>
            </TouchableOpacity>,
            <View style={{width:"100%", height:1, backgroundColor:"#d8d8d8", display:displayBorderBottom?"flex":"none"}}/>]
        );
    }

    return (
        [    <View style={{...styles.topBar}}>
            <TouchableOpacity>
                <Image style={{position:"absolute", bottom:0, left:16}} source={require(assestsPath+"back.png")}/>
            </TouchableOpacity>
            <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:20, color:cblack,}}>
                Settings
            </Text>
            <View style={{height:"100%", width:10}}/>
        </View>,

        <ScrollView style={{...styles.container}}  showsVerticalScrollIndicator={false}>
            <View style={{marginTop:windowHeight*0.0148, paddingLeft:30, paddingRight:30, backgroundColor:"#fff", width:windowWidth, flexDirection:"column", alignContent:"center", paddingTop:18, marginBottom:windowHeight*0.0148}}>
                {renderSettingsRowUserInfo(userInfo.name)}
                {renderSettingsRowUserInfo("id")}
                {renderSettingsRowUserInfo(userInfo.phone)}
                {renderSettingsRowUserInfo("Notifications", false, true, true)}
                {renderSettingsRowUserInfo("Manage all destinations")}
            </View>
            
            <View style={{paddingLeft:30, paddingRight:30, backgroundColor:"#fff", width:windowWidth, flexDirection:"column", alignContent:"center", marginBottom:10}}>
                {renderSettingsRowAppInfo("About Us")}
                {renderSettingsRowAppInfo("Terms of Service")}
                {renderSettingsRowAppInfo("Privacy Policy")}
                {renderSettingsRowAppInfo("Help Centre")}
                {renderSettingsRowAppInfo("Language", true, false)}
            </View>

        </ScrollView>]
    )
}

export default Settings;

const styles = StyleSheet.create({
    container:{
        height:windowHeight,
        width:windowWidth,
        display:"flex",
        // justifyContent:"flex-start",
        flexDirection:"column",
    },
    topBar:{
        height:75,
        backgroundColor:"#fff",
        display:"flex",
        justifyContent:"space-between",
        flexDirection:"row",
        alignItems:"flex-end",
        alignContent:"center",
        paddingBottom:6,
    }
});
