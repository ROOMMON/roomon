import React, { useState, useEffect } from 'react';
import {FlatList, Animated, Text, View, StyleSheet, TextInput, Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback, Dimensions, ImageBackground} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types'
import {useSelector, useDispatch} from 'react-redux'
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/native';


const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const assestsPath = "../../../assets/MyRentals/";
const cpurple = "#5c42dc";
const cblack = "#252533";

const MyListing = () => {
    const [numOfListings, setNumOfListings] = useState(7);

    const renderTopBar = () =>{
        return(
            <View style={{...styles.topBar}}>
                <Image source={require(assestsPath+"logo.png")}/>

                <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:20, color:cpurple}}>My Rentals</Text>

                <TouchableOpacity  onPress={()=>{}}>
                    <Image source={require(assestsPath+"hamburger.png")}/>
                </TouchableOpacity>
            </View>
        );
    }

    const renderListingCard = (listing={}) =>{
        return(
            <TouchableOpacity style={{display:"flex", width:"100%", flexDirection:"column", alignItems:"center", justifyContent:"flex-start", alignItems:"center", alignContent:"center", ...styles.boxShadow, backgroundColor:"#fff", marginBottom:20}}>
                <Image style={{width:"100%", height:240}} source={require(assestsPath+"dummy.png")}/>
                <View style={{height:26, width:"100%", marginTop:10, justifyContent:"space-around", alignContent:"center", alignItems:"center", flexDirection:"row"}}>
                    <Text style={{fontFamily:"Roboto-Regular", fontSize:16, color:cblack}}>{`${listing.bed} Bed`}</Text>
                    <View style={{height:"90%", width:1, backgroundColor:"#dddddd"}}/>
                    <Text style={{fontFamily:"Roboto-Regular", fontSize:16, color:cblack}}>{`${listing.bath} Bath`}</Text>
                    <View style={{height:"90%", width:1, backgroundColor:"#dddddd"}}/>
                    <Text style={{fontFamily:"Roboto-Regular", fontSize:16, color:cblack}}>{`${listing.moveInDate}`}</Text>
                    <View style={{height:"90%", width:1, backgroundColor:"#dddddd"}}/>
                    <Text style={{fontFamily:"Roboto-Regular", fontSize:16, color:cblack}}>{`${listing.sq} sq`}</Text>
                </View>
                <View style={{width:"100%", height:70, paddingBottom:20, justifyContent:"flex-start", alignContent:"center", alignItems:"center", paddingLeft:10, paddingRight:10, flexDirection:"row", marginTop:5}}>
                    <Text style={{fontFamily:"Roboto-Light", fontSize:14, color:"#36364c"}}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc imperdiet risus vitae …
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    const renderMyListingPageBody = () =>{
        let content;
        // let initialMessage
        if(numOfListings)
        {
            content=[];
            [...new Array(numOfListings)].forEach((v,i)=>{
                content.push(
                    renderListingCard({bed:1, bath:0, moveInDate:"Feb 1", sq:500})
                );
            });
        }
        else
        {
            content = [
                <View>
                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:22, color:cpurple, marginBottom:15}}>
                        You haven’t published a listing yet
                    </Text>

                    <Text style={{fontFamily:"Roboto-Light", fontSize:16, color:cblack, marginBottom:46}}>
                        Save time by renting your place with Roomon and contract safely!
                    </Text>
                </View>,
                <TouchableOpacity style={{alignSelf:"center",width:280, height:55, backgroundColor:cpurple, borderWidth:0.5, borderColor:cpurple, borderRadius:27.5, display:"flex", justifyContent:"center", alignContent:"center", alignItems:"center"}}>
                    <Text style={{fontFamily:"ArchivoBlack-Regular", fontSize:18, color:"#fff"}}>
                        Publish New Listing
                    </Text>
                </TouchableOpacity>]
        }
        return(
            <ScrollView style={{marginTop:numOfListings? 47 : 58 ,display:"flex"}}>
                {content}
            </ScrollView>
        );
    }

    return (
        <View style={{...styles.container}}>
            {renderTopBar()}

            <TouchableOpacity style={{flexDirection:"row", alignContent:"center", alignItems:"center", alignSelf:"center", marginBottom:15, marginTop:30 , display:numOfListings?"flex":"none"}}>
                <Image source={require(assestsPath+"plusActive.png")}/>
                <Text style={{marginLeft:10 ,fontFamily:"ArchivoBlack-Regular", fontSize:18, color:"#000"}}>
                    Publish New Listing
                </Text>
            </TouchableOpacity>

            <FlatList
                data={[1]}
                renderItem={() => renderMyListingPageBody()}
                contentContainerStyle={{bottom:30, paddingBottom:150}}
                style={{flexDirection:"column",}}
                showsVerticalScrollIndicator={false}/>
            
        </View>
    )
}

export default MyListing

const styles = StyleSheet.create({
    container:{
        display:"flex",
        paddingTop:40,
        paddingLeft:16,
        paddingRight:15,
        
    },
    topBar:{
        display:"flex",
        flexDirection:"row",
        alignContent:"center",
        alignItems:"center",
        justifyContent:"space-between",
    },
    boxShadow:{
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 1
    }
});