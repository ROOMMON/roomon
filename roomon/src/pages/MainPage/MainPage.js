import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import Login from "../SignInUp/LogIn"
import Home from "../SignInUp/Home"
import Agreement from "../SignInUp/Agreement"
import AsyncStorage from '@react-native-community/async-storage';

MainPage = React.memo(function MainPage(props){
    const dispatch = useDispatch()
    
    const [hasAgreed, setHasAgreed] = useState(false)
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    
const _getDeviceStorage = async () => {
      
    // await AsyncStorage.setItem(
    //   'agreement',
    //   JSON.stringify({ agreement: false}),
    // );
    let values;
    try {
      await AsyncStorage.multiGet(['agreement', 'isLoggedIn'], (err, result) => {
        if(result){
          result.map((result, i, store) => {
            // get at each store's key/value so you can work with it
            let key = store[i][0];
            let value = store[i][1];
            if(value && key) {
              if(key === 'agreement'){
                const parse = JSON.parse(value)
                if(parse) {
                    console.info("agreement status: ",parse.agreement)
                  setHasAgreed(parse.agreement)
                }else {
                  console.error("ERROR: Agreement._getDeviceStorage parse.agreement should not be nul")
                }
              } else if(key === 'isLoggedIn'){
                const parse=JSON.parse(value)
                if(parse) {
                  setIsLoggedIn(parse.isLoggedIn)
                }else {
                  console.error("ERROR: Agreement._getDeviceStorage parse.isLoggedIn should not be nul")
                }
              
              }
            }
            
            
          });
        }
      })
    } catch (e) {
      console.log("ERROR: MainPage._getDeviceStorage: ", e)
    }
      // console.log(result.agreement)
    //   Object.values(result).map( value =>{
                
        
    //   console.log(value.ag)
    // })
      
  }
  useEffect(() => {
    _getDeviceStorage()
  },[])
  console.log("==========")
  console.log("isLoggedIn: ", isLoggedIn, "hasAgreed: ", hasAgreed)
  console.log("==========")
    if(isLoggedIn && hasAgreed){
        return <Home {...props}/>
    } else if(!isLoggedIn && hasAgreed) {
        return <Login {...props}/>
    } else {
        return <Agreement {...props}/>
    }


});

// const styles = StyleSheet.create({
    
// });

export default MainPage;