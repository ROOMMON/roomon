import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TextInput, Image, TouchableOpacity} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types'

//Reusable components
import DefaultContainer from '../DefaultContainer';

const assestsPath = "../../../assets/afterSignUp/";

export default CitySearchBar = React.memo(function CitySearchBar(props){


    const [search, setSearch] = useState("");

    const handleViewOnMap = () =>{
        if(PAGE_SETTINGS[props.pageType].viewOnMap)
            return (
            <TouchableOpacity onPress={() => alert("OK")}>
                <View style={{flexDirection:"row", alignItems:"center", justifyContent:"space-between", marginBottom:26,}}>
                    <Text style={{color:"#252533", fontWeight:"bold"}}>View on Map</Text>
                    <Image source={require(assestsPath+"arrowPointRight.png")}/>
                </View>
            </TouchableOpacity>
            );
        return null;
    }

    const searchBar = () =>{
        return (
            <View style={{...styles.searchBar, marginTop:20, flexDirection:"row"}}>
            {handleSearchBarIcon()}
                <TextInput 
                placeholder={"Search"} 
                style={{...styles.searchBar, paddingLeft:80}}
                value={search}
                onChangeText={text => setSearch(text)}
                />
            </View>
        );
    }

    const searchMenu = () =>{
        if(search)
        {
            let matches = DUMMY_DATA.map((data, i)=>{
                let d = data["City"].toLowerCase();
                if(d.includes(search.toLowerCase()))
                {
                    return(
                        searchMenuItem(data["City"])
                    );
                }
            });

            return(
                <View style={{...styles.searchMenu}}>
                    {handleViewOnMap()}
                    <KeyboardAwareScrollView style={{height:"100%", overflow:"scroll"}}>
                        {matches}
                    </KeyboardAwareScrollView>
                        
                </View>
            ); 
        }
        return null;
    }

    const searchMenuItem = (item="") =>{
        return(
            <TouchableOpacity onPress={()=>alert("OK: " + item)}>
                <View style={{...styles.searchMenuItem}}>
                    <Image source={require(assestsPath+"searchMenuItem.png")} style={{marginRight:13}}/>
                    <Text style={{fontSize:16, color:"#252533",}}>{item}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <DefaultContainer 
        setCurrentPage={()=>props.navigation.navigate("AfterSignUp")}
        NavBarMiddleCustomText={props.pageType} 
        NavBarMiddleContent={"custom"}
        showSkip={false}>
        
        {searchBar()}
        {searchMenu()}

        </DefaultContainer>
    );
});

CitySearchBar.PropTypes = {
    pageType: PropTypes.string,
  };

CitySearchBar.defaultProps = {
    pageType: "City"
  };

const styles = StyleSheet.create({
    searchBar:{
        width: "100%",
        height:45,
        borderStyle:"solid",
        borderColor:"#d8d8d8",
        borderWidth: 1,
        marginBottom:20,
    },
    searchMenu:{
        width:"100%",

    },
    searchMenuItem:{
        width:"100%",
        height:30,
        flexDirection:"row",
        // alignContent:"center",
        alignItems:"center",
        marginBottom: 20
    }
});


const DUMMY_DATA = [
    {"City": "Downtown Vancouver"},
    {"City": "Surrey"},
    {"City": "Burnaby"},
    {"City": "Richmond"},
    {"City": "Coquitlam"},
    {"City": "West Vancouver"},
    {"City": "Langley City"},
    {"City": "North Vancouver"},
    {"City": "Port Moody"},
    {"City": "Delta"},
    {"City": "White Rock"},
    {"City": "Pitt Meadows"},
    {"City": "Metrotown"},
];


