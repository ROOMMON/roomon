import React, { useState, useEffect, useRef } from 'react';
import { Text, View, StyleSheet, Dimensions, TextInput, Image, TouchableOpacity, FlatList, SafeAreaView, PixelRatio, Platform} from 'react-native'

import DefaultContainer from './DefaultContainer';
import MapboxGL from "@react-native-mapbox-gl/maps";
import PulseCircleLayer from './PulseCircleLayer'
MapboxGL.setAccessToken('pk.eyJ1Ijoic2hpbm1pbmplIiwiYSI6ImNrOXp2NjdreDBqZzIzb292Z2JxdnhzMW0ifQ.75jKzFuA49cCcAsQmB_oew');
MapboxGL.setTelemetryEnabled(false)

const assestsPath = "../../../assets/afterSignUp/";
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

export default MapView = React.memo(function MapView(props){

    const [place, setPlace] = useState({})
    const [place_coordinate, setPlace_coordinate] = useState([-123.038377, 49.246813])
    const place_id = ""
    const [centerLocation, setCenterLocation] = useState()
    
    const defaultCoordinate = [-123.038377, 49.246813]

    const cameraReposition = async () => {
      await setPlace_coordinate(place.place_coordinate)
      const zoom =await this.cameraRef.setCamera({
        centerCoordinate: place.place_coordinate,
        animationMode: "easTo",
        animationDuration: 3800,
        zoomLevel: 12

      })
    }
    useEffect(()=>{
      if(props.selectedPlace.place_coordinate && props.selectedPlace.place_coordinate.length === 2){
        setPlace(props.selectedPlace)
        cameraReposition()
      }
    })
  
    return (
      <>    
        
        <View style={styles.container}>
          <MapboxGL.MapView 
            ref={(ref) => (this._map = ref)}
            style={styles.map} 
            rotateEnabled={false}
            logoEnabled={false}
          >
          <MapboxGL.Camera
            ref={(camera) => (this.cameraRef = camera)}
            defaultSettings={{
              centerCoordinate: defaultCoordinate,
              zoomLevel: 9,
            }}
          />
          {place.place_coordinate && place.place_coordinate.length === 2 && (
            
            <MapboxGL.PointAnnotation
            key={"id"}
            id={"id"}
            coordinate={place.place_coordinate}
            title={"title"}>
              <PulseCircleLayer/>
              <MapboxGL.Callout title={place.place_name} />
              
            </MapboxGL.PointAnnotation>
          )}
          
          </MapboxGL.MapView>
          
        </View>
     </>
    )
    

});
const ANNOTATION_SIZE = 25;
const styles = StyleSheet.create({
    container: {
      marginTop: 10,
      height: windowHeight,
      width: windowWidth,
    },
    map: {
      flex: 1
    }
  });